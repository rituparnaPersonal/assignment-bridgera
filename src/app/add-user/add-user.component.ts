import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FILE } from 'dns';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  userDetails: any = [];
  userArr: any = [];
  constructor(
    private route: Router
  ) { }

  ngOnInit(): void {
    let userStr: any = localStorage.getItem('Details');
    this.userArr = JSON.parse(userStr);
    if(localStorage.getItem('Details')){
      this.userDetails = this.userArr;
    }
    this.setid()
  }

  userForm = new FormGroup(
    {
      id:new FormControl(''),
      name : new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" )]),
      address : new FormControl('', Validators.required),
      class: new FormControl('', Validators.required),
      phone: new FormControl(null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
      image:new FormControl(''),
    }
    
  )

  setid(){
    let number = new Date().getTime();
    this.userForm.get('id')?.setValue(number)
  }

  Submit(){
    console.log(this.userDetails)
    this.userDetails.push(this.userForm.value)
    localStorage.setItem('Details',JSON.stringify(this.userDetails))
    console.log('Add Value: ', this.userDetails)
    this.route.navigate(['/user-listing'])
    
  }

}
