import { Component, OnInit } from '@angular/core';
import { Router ,NavigationExtras} from '@angular/router';
@Component({
  selector: 'app-userlisting',
  templateUrl: './userlisting.component.html',
  styleUrls: ['./userlisting.component.scss']
})
export class UserlistingComponent implements OnInit {
  userListing:any
  constructor(
    private route: Router
  ) { }

  ngOnInit(): void {
    var userlisting:any = localStorage.getItem('Details')
    this.userListing = (localStorage.getItem('Details'))? JSON.parse(userlisting):[];
  }

  goToEdit(item:any){
    this.route.navigate(['/edit-user',item.id]);
  }
  
  deleteUser(index:any){
    if(confirm("Are you want to delete")) {
      this.userListing.splice(index,1);
      localStorage.setItem('Details',JSON.stringify(this.userListing));
      /////// Here I use index value for delete user. We can also use ID to delete user after finding index value.
    }
  }

}
