import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap ,Router} from '@angular/router';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  userID:any;
  userDetails : any ;
  usersArr: any = [];
  userForm = new FormGroup(
    {
      id:new FormControl(''),
      name : new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" )]),
      address : new FormControl('', Validators.required),
      class: new FormControl('', Validators.required),
      phone: new FormControl(null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
      image:new FormControl(''),
    })
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router
  ) {
    this.userID=this.activatedRoute.snapshot.paramMap.get('id');
   
    let userId = this.userID;
    let userString : any = localStorage.getItem('Details');
    this.usersArr = JSON.parse(userString);
    this.userDetails = this.usersArr.find((u: any)=> u.id == userId);
    console.log(this.userDetails)

   }

   

  ngOnInit(): void {

    this.userForm.patchValue({
      "id": this.userDetails.id,
      "name" : this.userDetails.name,
      "email" : this.userDetails.email,
      "address" : this.userDetails.address,
      "class" : this.userDetails.class,
      "phone" : this.userDetails.phone

  });
    
  }

  Submit(){
    let userId = this.userID;
    let userString : any = localStorage.getItem('Details');
    this.usersArr = JSON.parse(userString);
    console.log(this.usersArr,'-------------------')
    let arr: any = this.usersArr.find((u: any)=> u.id == userId);
    console.log(arr)
    if(arr){
      let index = this.usersArr.indexOf(arr) 
      this.usersArr[index] = this.userForm.value; 
      localStorage.setItem('Details',JSON.stringify(this.usersArr))
      console.log(this.usersArr)

      this.route.navigate(['/user-listing'])
    }else{
    }

  }

}
